/*
 * Pidgin plugin for USB headset management
 *
 * Copyright © 2018 David Woodhouse.
 *
 * Author: David Woodhouse <dwmw2@infradead.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef __HEADSET_H__
#define __HEADSET_H__

/* headset.c */
void headset_muted(gpointer user_data, gboolean mute);
void headset_connected(gpointer user_data, gboolean connected);


/* jabra.c */
void mute_headset(gpointer user_data, gboolean muted);
void connect_headset(gpointer user_data, gboolean connected);
gboolean init_headset(void);
void shutdown_headset(void);

#endif /* __HEADSET_H__ */
